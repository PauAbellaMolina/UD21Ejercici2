package UD21Ejercicio2.UD21Ejercicio2;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import methods.MethodsCalc;

import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextArea;

public class Ejercicio2App extends JFrame {

	private JPanel contentPane;
	public JButton num9;
	public JButton num8;
	public JButton num7;
	public JButton num6;
	public JButton num5;
	public JButton num4;
	public JButton num3;
	public JButton num2;
	public JButton num1;
	public JButton num0;
	public static JLabel result;
	public static JLabel resultAux;
	public static JTextArea textAreaHistorial;
	public static double calc1;
	public static double calc2;
	public static String operation;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio2App frame = new Ejercicio2App();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ejercicio2App() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 827, 568);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		num9 = new JButton("9");
		num9.setBounds(239, 211, 89, 23);
		contentPane.add(num9);
		num9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				result.setText(result.getText() + "9");
			}
		});
		
		num8 = new JButton("8");
		num8.setBounds(140, 211, 89, 23);
		contentPane.add(num8);
		num8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				result.setText(result.getText() + "8");
			}
		});
		
		num7 = new JButton("7");
		num7.setBounds(41, 211, 89, 23);
		contentPane.add(num7);
		num7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				result.setText(result.getText() + "7");
			}
		});
		
		num6 = new JButton("6");
		num6.setBounds(41, 245, 89, 23);
		contentPane.add(num6);
		num6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				result.setText(result.getText() + "6");
			}
		});
		
		num5 = new JButton("5");
		num5.setBounds(141, 245, 89, 23);
		contentPane.add(num5);
		num5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				result.setText(result.getText() + "5");
			}
		});
		
		num4 = new JButton("4");
		num4.setBounds(239, 244, 89, 23);
		contentPane.add(num4);
		num4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				result.setText(result.getText() + "4");
			}
		});
		
		num3 = new JButton("3");
		num3.setBounds(41, 280, 89, 23);
		contentPane.add(num3);
		num3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				result.setText(result.getText() + "3");
			}
		});
		
		num2 = new JButton("2");
		num2.setBounds(141, 280, 89, 23);
		contentPane.add(num2);
		num2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				result.setText(result.getText() + "2");
			}
		});
		
		num1 = new JButton("1");
		num1.setBounds(238, 280, 89, 23);
		contentPane.add(num1);
		num1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				result.setText(result.getText() + "1");
			}
		});
		
		num0 = new JButton("0");
		num0.setBounds(142, 312, 89, 23);
		contentPane.add(num0);
		num0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				result.setText(result.getText() + "0");
			}
		});
		
		JButton btnComma = new JButton(",");
		btnComma.setBounds(238, 312, 89, 23);
		contentPane.add(btnComma);
		btnComma.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				result.setText(result.getText() + ".");
			}
		});
		
		final JButton btnEquals = new JButton("=");
		btnEquals.setBounds(335, 312, 89, 23);
		contentPane.add(btnEquals);
		btnEquals.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calc2 = Double.parseDouble(result.getText());
				MethodsCalc.calculate(calc1, calc2, operation);
			}
		});
		
		final JButton btnPlus = new JButton("+");
		btnPlus.setBounds(335, 280, 89, 23);
		contentPane.add(btnPlus);
		btnPlus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calc1 = Double.parseDouble(result.getText());
				operation = "+";
				resultAux.setText(result.getText() + "+");
				result.setText("");
			}
		});
		
		JButton btnLess = new JButton("-");
		btnLess.setBounds(335, 245, 89, 23);
		contentPane.add(btnLess);
		btnLess.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calc1 = Double.parseDouble(result.getText());
				operation = "-";
				resultAux.setText(result.getText() + "-");
				result.setText("");
			}
		});
		
		JButton btnTimes = new JButton("x");
		btnTimes.setBounds(335, 211, 89, 23);
		contentPane.add(btnTimes);
		btnTimes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calc1 = Double.parseDouble(result.getText());
				operation = "x";
				resultAux.setText(result.getText() + "x");
				result.setText("");
			}
		});
		
		JButton btnDiv = new JButton("/");
		btnDiv.setBounds(335, 179, 89, 23);
		contentPane.add(btnDiv);
		btnDiv.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calc1 = Double.parseDouble(result.getText());
				operation = "/";
				resultAux.setText(result.getText() + "/");
				result.setText("");
			}
		});
		
		JButton btnSqrd = new JButton("√");
		btnSqrd.setBounds(239, 179, 89, 23);
		contentPane.add(btnSqrd);
		btnSqrd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calc1 = Double.parseDouble(result.getText());
				operation = "√";
				resultAux.setText(result.getText() + "√");
				result.setText("");
				calc2 = 1;
				MethodsCalc.calculate(calc1, calc2, operation);
			}
		});
		
		JButton btnPower = new JButton("x²");
		btnPower.setBounds(140, 179, 89, 23);
		contentPane.add(btnPower);
		btnPower.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calc1 = Double.parseDouble(result.getText());
				operation = "x²";
				resultAux.setText(result.getText() + "x²");
				result.setText("");
				calc2 = 1;
				MethodsCalc.calculate(calc1, calc2, operation);
			}
		});
		
		JButton btn1x = new JButton("¹/x");
		btn1x.setBounds(41, 179, 89, 23);
		contentPane.add(btn1x);
		btn1x.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calc1 = Double.parseDouble(result.getText());
				operation = "¹/x";
				resultAux.setText(result.getText() + "¹/x");
				result.setText("");
				calc2 = 1;
				MethodsCalc.calculate(calc1, calc2, operation);
			}
		});
		
		JButton btnReturn = new JButton("Del");
		btnReturn.setBounds(335, 148, 89, 23);
		contentPane.add(btnReturn);
		btnReturn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				result.setText(result.getText().substring(0, result.getText().length() - 1));
			}
		});
		
		JButton btnPercent = new JButton("%");
		btnPercent.setBounds(41, 148, 89, 23);
		contentPane.add(btnPercent);
		btnPercent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calc1 = Double.parseDouble(result.getText());
				operation = "%";
				resultAux.setText(result.getText() + "%");
				result.setText("");
			}
		});
		
		JButton btnCE = new JButton("CE");
		btnCE.setBounds(140, 148, 89, 23);
		contentPane.add(btnCE);
		btnCE.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				result.setText("");
			}
		});
		
		
		JButton btnC = new JButton("C");
		btnC.setBounds(239, 148, 89, 23);
		contentPane.add(btnC);
		btnC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultAux.setText("");
				result.setText("");
			}
		});
		
		JButton btnMod = new JButton("mod");
		btnMod.setBounds(335, 367, 89, 23);
		contentPane.add(btnMod);
		btnMod.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calc1 = Double.parseDouble(result.getText());
				operation = "mod";
				resultAux.setText(result.getText() + " mod");
				result.setText("");
			}
		});
		
		result = new JLabel("");
		result.setFont(new Font("Tahoma", Font.PLAIN, 30));
		result.setBounds(41, 76, 383, 30);
		contentPane.add(result);
		
		resultAux = new JLabel("");
		resultAux.setFont(new Font("Tahoma", Font.PLAIN, 30));
		resultAux.setBounds(41, 35, 383, 30);
		contentPane.add(resultAux);
		
		JLabel lblHistorial = new JLabel("Historial");
		lblHistorial.setBounds(528, 33, 237, 14);
		contentPane.add(lblHistorial);
		
		textAreaHistorial = new JTextArea();
		textAreaHistorial.setEditable(false);
		textAreaHistorial.setBounds(528, 57, 237, 432);
		contentPane.add(textAreaHistorial);
		
		JButton btnExp = new JButton("exp");
		btnExp.setBounds(335, 402, 89, 23);
		contentPane.add(btnExp);
		btnExp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calc1 = Double.parseDouble(result.getText());
				operation = "exp";
				resultAux.setText(result.getText() + " exp ");
				result.setText("");
			}
		});
		
		JButton btnPi = new JButton("π");
		btnPi.setBounds(239, 367, 89, 23);
		contentPane.add(btnPi);
		btnPi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultAux.setText("3.14159265358979323846");
				result.setText("3.14159265358979323846");
			}
		});
		
		JButton btnN = new JButton("n!");
		btnN.setBounds(239, 402, 89, 23);
		contentPane.add(btnN);
		btnN.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calc1 = Double.parseDouble(result.getText());
				operation = "n!";
				resultAux.setText(result.getText() + "!");
				result.setText("");
				calc2 = 1;
				MethodsCalc.calculate(calc1, calc2, operation);
			}
		});
		
		JButton btnAbs = new JButton("|x|");
		btnAbs.setBounds(41, 402, 89, 23);
		contentPane.add(btnAbs);
		btnAbs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calc1 = Double.parseDouble(result.getText());
				operation = "|x|";
				resultAux.setText(result.getText() + " |x|");
				result.setText("");
				calc2 = 1;
				MethodsCalc.calculate(calc1, calc2, operation);
			}
		});
		
		JButton btnXy = new JButton("xʸ");
		btnXy.setBounds(140, 402, 89, 23);
		contentPane.add(btnXy);
		btnXy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calc1 = Double.parseDouble(result.getText());
				operation = "xʸ";
				resultAux.setText(result.getText() + "xʸ");
				result.setText("");
			}
		});
		
		JButton btnLog = new JButton("log");
		btnLog.setBounds(41, 367, 89, 23);
		contentPane.add(btnLog);
		btnLog.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calc1 = Double.parseDouble(result.getText());
				operation = "log";
				resultAux.setText("log " + result.getText());
				result.setText("");
				calc2 = 1;
				MethodsCalc.calculate(calc1, calc2, operation);
			}
		});
		
		JButton btnLn = new JButton("ln");
		btnLn.setBounds(140, 367, 89, 23);
		contentPane.add(btnLn);
		btnLn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calc1 = Double.parseDouble(result.getText());
				operation = "ln";
				resultAux.setText("ln " + result.getText());
				result.setText("");
				calc2 = 1;
				MethodsCalc.calculate(calc1, calc2, operation);
			}
		});
		
		JButton btnSin = new JButton("sin");
		btnSin.setBounds(140, 446, 89, 23);
		contentPane.add(btnSin);
		btnSin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calc1 = Double.parseDouble(result.getText());
				operation = "sin";
				resultAux.setText("sin " + result.getText());
				result.setText("");
				calc2 = 1;
				MethodsCalc.calculate(calc1, calc2, operation);
			}
		});
		
		JButton btnCos = new JButton("cos");
		btnCos.setBounds(239, 446, 89, 23);
		contentPane.add(btnCos);
		btnCos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calc1 = Double.parseDouble(result.getText());
				operation = "cos";
				resultAux.setText("cos " + result.getText());
				result.setText("");
				calc2 = 1;
				MethodsCalc.calculate(calc1, calc2, operation);
			}
		});
		
		JButton btnTan = new JButton("tan");
		btnTan.setBounds(335, 446, 89, 23);
		contentPane.add(btnTan);
		btnTan.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calc1 = Double.parseDouble(result.getText());
				operation = "tan";
				resultAux.setText("tan " + result.getText());
				result.setText("");
				calc2 = 1;
				MethodsCalc.calculate(calc1, calc2, operation);
			}
		});
	}
}
