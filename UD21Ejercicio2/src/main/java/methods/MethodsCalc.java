package methods;

import UD21Ejercicio2.UD21Ejercicio2.Ejercicio2App;

public class MethodsCalc {	
	public static double calculate(double calc1, double calc2, String operation) {
		switch (operation) {
			case "+":
				Ejercicio2App.result.setText(String.valueOf(calc1+calc2));
				Ejercicio2App.resultAux.setText(String.valueOf(calc1+calc2));
				Ejercicio2App.textAreaHistorial.setText(Ejercicio2App.textAreaHistorial.getText() + calc1 + " + " + calc2 + " = " + String.valueOf(calc1+calc2) + "\n");
				return calc1+calc2;
			case "-":
				Ejercicio2App.result.setText(String.valueOf(calc1-calc2));
				Ejercicio2App.resultAux.setText(String.valueOf(calc1-calc2));
				Ejercicio2App.textAreaHistorial.setText(Ejercicio2App.textAreaHistorial.getText() + calc1 + " - " + calc2 + " = " + String.valueOf(calc1-calc2) + "\n");
				return calc1-calc2;
			case "x":
				Ejercicio2App.result.setText(String.valueOf(calc1*calc2));
				Ejercicio2App.resultAux.setText(String.valueOf(calc1*calc2));
				Ejercicio2App.textAreaHistorial.setText(Ejercicio2App.textAreaHistorial.getText() + calc1 + " x " + calc2 + " = " + String.valueOf(calc1*calc2) + "\n");
				return calc1*calc2;
			case "/":
				Ejercicio2App.result.setText(String.valueOf(calc1/calc2));
				Ejercicio2App.resultAux.setText(String.valueOf(calc1/calc2));
				Ejercicio2App.textAreaHistorial.setText(Ejercicio2App.textAreaHistorial.getText() + calc1 + " / " + calc2 + " = " + String.valueOf(calc1/calc2) + "\n");
				return calc1/calc2;
			case "√":
				Ejercicio2App.result.setText(String.valueOf(Math.sqrt(calc1)));
				Ejercicio2App.resultAux.setText(String.valueOf(Math.sqrt(calc1)));
				Ejercicio2App.textAreaHistorial.setText(Ejercicio2App.textAreaHistorial.getText() + " √" + calc1 + " = " + String.valueOf(Math.sqrt(calc1)) + "\n");
				return Math.sqrt(calc1);
			case "x²":
				Ejercicio2App.result.setText(String.valueOf(Math.pow(calc1, 2)));
				Ejercicio2App.resultAux.setText(String.valueOf(Math.pow(calc1, 2)));
				Ejercicio2App.textAreaHistorial.setText(Ejercicio2App.textAreaHistorial.getText() + calc1 + "² = " + String.valueOf(Math.pow(calc1, 2)) + "\n");
				return Math.pow(calc1, 2);
			case "¹/x":
				Ejercicio2App.result.setText(String.valueOf(1/calc1));
				Ejercicio2App.resultAux.setText(String.valueOf(1/calc1));
				Ejercicio2App.textAreaHistorial.setText(Ejercicio2App.textAreaHistorial.getText() + "¹/" + calc1 + " = " + String.valueOf(1/calc1) + "\n");
				return 1/calc1;
			case "%":
				Ejercicio2App.result.setText(String.valueOf(calc1/100 * calc2));
				Ejercicio2App.resultAux.setText(String.valueOf(calc1/100 * calc2));
				Ejercicio2App.textAreaHistorial.setText(Ejercicio2App.textAreaHistorial.getText() + calc1 + "% de " + calc2 + " = " + String.valueOf(calc1/100 * calc2) + "\n");
				return calc1/100 * calc2;
			case "log":
				Ejercicio2App.result.setText(String.valueOf(Math.log(calc1)));
				Ejercicio2App.resultAux.setText(String.valueOf(Math.log(calc1)));
				Ejercicio2App.textAreaHistorial.setText(Ejercicio2App.textAreaHistorial.getText() + "log " + calc1 + " = " + String.valueOf(Math.log(calc1)) + "\n");
				return Math.log(calc1);
			case "ln":
				Ejercicio2App.result.setText(String.valueOf((-Math.log(1-calc1))/calc1));
				Ejercicio2App.resultAux.setText(String.valueOf((-Math.log(1-calc1))/calc1));
				Ejercicio2App.textAreaHistorial.setText(Ejercicio2App.textAreaHistorial.getText() + "ln " + calc1 + " = " + String.valueOf((-Math.log(1-calc1))/calc1) + "\n");
				return (-Math.log(1-calc1))/calc1;
			case "sin":
				Ejercicio2App.result.setText(String.valueOf(Math.sin(Math.toRadians(calc1))));
				Ejercicio2App.resultAux.setText(String.valueOf(Math.sin(Math.toRadians(calc1))));
				Ejercicio2App.textAreaHistorial.setText(Ejercicio2App.textAreaHistorial.getText() + "sin " + calc1 + " = " + String.valueOf(Math.sin(Math.toRadians(calc1))) + "\n");
				return Math.sin(Math.toRadians(calc1));
			case "cos":
				Ejercicio2App.result.setText(String.valueOf(Math.cos(Math.toRadians(calc1))));
				Ejercicio2App.resultAux.setText(String.valueOf(Math.cos(Math.toRadians(calc1))));
				Ejercicio2App.textAreaHistorial.setText(Ejercicio2App.textAreaHistorial.getText() + "cos " + calc1 + " = " + String.valueOf(Math.cos(Math.toRadians(calc1))) + "\n");
				return Math.cos(Math.toRadians(calc1));
			case "tan":
				Ejercicio2App.result.setText(String.valueOf(Math.tan(Math.toRadians(calc1))));
				Ejercicio2App.resultAux.setText(String.valueOf(Math.tan(Math.toRadians(calc1))));
				Ejercicio2App.textAreaHistorial.setText(Ejercicio2App.textAreaHistorial.getText() + "tan " + calc1 + " = " + String.valueOf(Math.tan(Math.toRadians(calc1))) + "\n");
				return Math.tan(Math.toRadians(calc1));
			case "mod":
				Ejercicio2App.result.setText(String.valueOf(calc1%calc2));
				Ejercicio2App.resultAux.setText(String.valueOf(calc1%calc2));
				Ejercicio2App.textAreaHistorial.setText(Ejercicio2App.textAreaHistorial.getText() + calc1 + " mod " + calc2 + " = " + String.valueOf(calc1%calc2) + "\n");
				return calc1%calc2;
			case "|x|":
				Ejercicio2App.result.setText(String.valueOf(calc1));
				Ejercicio2App.resultAux.setText(String.valueOf(calc1));
				Ejercicio2App.textAreaHistorial.setText(Ejercicio2App.textAreaHistorial.getText() + calc1 + " |x| = " + String.valueOf(calc1) + "\n");
				return calc1;
			case "xʸ":
				Ejercicio2App.result.setText(String.valueOf(Math.pow(calc1, calc2)));
				Ejercicio2App.resultAux.setText(String.valueOf(Math.pow(calc1, calc2)));
				Ejercicio2App.textAreaHistorial.setText(Ejercicio2App.textAreaHistorial.getText() + calc1 + " ^ " + calc2 + " = " + String.valueOf(Math.pow(calc1, calc2)) + "\n");
				return Math.pow(calc1, calc2);
			case "n!":
				double auxN = calc1;
				double result = calc1;
				calc1--;
				for (calc1 = calc1;calc1 > 0; calc1--) {
					result*=calc1;
				}
				Ejercicio2App.result.setText(String.valueOf(result));
				Ejercicio2App.resultAux.setText(String.valueOf(result));
				Ejercicio2App.textAreaHistorial.setText(Ejercicio2App.textAreaHistorial.getText() + auxN + " ! = " + String.valueOf(result) + "\n");
				return result;
			case "exp":	
				double auxExp = calc2;
				double resultExp = calc1;
				for (calc2 = calc2;calc2 > 0; calc2--) {
					resultExp*=10;
				}
				Ejercicio2App.result.setText(String.valueOf(resultExp));
				Ejercicio2App.resultAux.setText(String.valueOf(resultExp));
				Ejercicio2App.textAreaHistorial.setText(Ejercicio2App.textAreaHistorial.getText() + calc1 + " exp " + auxExp + "= " + String.valueOf(resultExp) + "\n");
				return resultExp;
			default:
				return 0;
		}
	}
}
