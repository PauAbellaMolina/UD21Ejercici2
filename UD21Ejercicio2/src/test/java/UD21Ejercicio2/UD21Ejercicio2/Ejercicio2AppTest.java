package UD21Ejercicio2.UD21Ejercicio2;

import methods.MethodsCalc;
import junit.framework.TestCase;

/**
 * Unit test for simple App.
 */
public class Ejercicio2AppTest extends TestCase {
	public void testSuma() {
		double resultado = MethodsCalc.calculate(2, 2, "+");
		double esperado = 4;
		assertEquals(esperado, resultado);
	}
	
	public void testResta() {
		double resultado = MethodsCalc.calculate(2, 2, "-");
		double esperado = 0;
		assertEquals(esperado, resultado);
	}
	
	public void testMult() {
		double resultado = MethodsCalc.calculate(2, 2, "x");
		double esperado = 4;
		assertEquals(esperado, resultado);
	}
	
	public void testDiv() {
		double resultado = MethodsCalc.calculate(4, 2, "/");
		double esperado = 2;
		assertEquals(esperado, resultado);
	}
	
	public void testSqrd() {
		double resultado = MethodsCalc.calculate(16, 0, "√");
		double esperado = 4;
		assertEquals(esperado, resultado);
	}
	
	public void testElev() {
		double resultado = MethodsCalc.calculate(2, 0, "x²");
		double esperado = 4;
		assertEquals(esperado, resultado);
	}
	
	public void test1DivX() {
		double resultado = MethodsCalc.calculate(2, 0, "¹/x");
		double esperado = 0.5;
		assertEquals(esperado, resultado);
	}
	
	public void testPorc() {
		double resultado = MethodsCalc.calculate(10, 100, "%");
		double esperado = 10;
		assertEquals(esperado, resultado);
	}
	
	public void testLog() {
		double resultado = MethodsCalc.calculate(2, 0, "log");
		double esperado = 0.6931471805599453;
		assertEquals(esperado, resultado);
	}
	
	public void testLn() {
		double resultado = MethodsCalc.calculate(2, 0, "ln");
		double esperado = 0.6931471805599453;
		assertEquals(esperado, resultado);
	}
	
	public void testSin() {
		double resultado = MethodsCalc.calculate(2, 0, "sin");
		double esperado = 0.03489949670250097;
		assertEquals(esperado, resultado);
	}
	
	public void testCos() {
		double resultado = MethodsCalc.calculate(2, 0, "cos");
		double esperado = 0.9993908270190958;
		assertEquals(esperado, resultado);
	}
	
	public void testTan() {
		double resultado = MethodsCalc.calculate(2, 0, "tan");
		double esperado = 0.03492076949174773;
		assertEquals(esperado, resultado);
	}
	
	public void testMod() {
		double resultado = MethodsCalc.calculate(5, 2, "mod");
		double esperado = 1;
		assertEquals(esperado, resultado);
	}
	
	public void testAbs() {
		double resultado = MethodsCalc.calculate(2, 0, "|x|");
		double esperado = 2;
		assertEquals(esperado, resultado);
	}
	
	public void testXy() {
		double resultado = MethodsCalc.calculate(2, 2, "xʸ");
		double esperado = 4;
		assertEquals(esperado, resultado);
	}
	
	public void testFact() {
		double resultado = MethodsCalc.calculate(2, 2, "n!");
		double esperado = 2;
		assertEquals(esperado, resultado);
	}
	
	public void testExp() {
		double resultado = MethodsCalc.calculate(2, 2, "exp");
		double esperado = 200;
		assertEquals(esperado, resultado);
	}
}
